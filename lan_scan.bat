@echo on

REM https://blog.gtwang.org/linux/ping-and-arp-scan-ip-mac-address-script/

arp -d
for /L %%i in (1,1,254) do (
	ping 192.168.55.%%i -n 1 -w 300 > NUL
)
arp -a | find "192.168"
pause